# Service Center
A simple service center web application to make equipment ordering easy and secure.
## Used libraries
- Frontend
  - React JS 16.0
  - Bootstrap 4.1
  - JQuery 3.4
  - RXJS 6.5
  - Formik 2.1
  - Yup 0.28
- Backend
  - Asp.Net Core 3.1
  - EntityFrameworkCore 3.1
  - SQLite 3
  - Swagger 5.0
  - Serilog 2.8
  - Newtonsoft.Json 12.0
  - AutoMapper 9.0
  - CryptoHelper 3.0