﻿import React, { Component } from 'react';
import { FormGroup, Label } from "reactstrap";
import { DatePicker } from 'react-datepicker';
export class MyDatePicker extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }
    handleChange = value => {
        this.props.onChange(this.props.id, value);
    };

    handleBlur = () => {
        this.props.onBlur(this.props.id, true);
    };

    render() {
        return (
            <DatePicker
                autoComplete="off"
                id={this.props.id}
                maxDate={this.props.maxDate || null}
                selected={
                    typeof this.props.value === 'string'
                        ? new Date(this.props.value)
                        : this.props.value
                }
                onBlur={this.handleBlur}
                dateFormat="DD/MM/YYYY"
                onChange={this.handleChange}
                showYearDropdown
                dateFormatCalendar="MMMM"
                scrollableYearDropdown
                disabled={this.props.disabled}
                yearDropdownItemNumber={15}
            />
        );
    }
}