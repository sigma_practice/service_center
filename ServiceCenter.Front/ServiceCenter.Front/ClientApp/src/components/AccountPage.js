﻿import React, { Component } from 'react';
import { authenticationService } from '../services/auth_service';
import { usersService } from '../services/service_users';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

export class AccountPage extends Component {
    static displayName = AccountPage.name;

    constructor() {
        super();

        this.state = {
            user: null
        }
    }

    getUser() {
        var token = authenticationService.currentUserValue.accessToken;
        if (token) {
            return usersService.getUser(token);
        }
        return null;
    }

    componentDidMount() {
        if (authenticationService.currentUserValue) {
            // fetch the user, once it retrieves resolve the promsie and update the state. 
            this.getUser().then(result => this.setState({
                user: result
            }));
        }
        else {
            authenticationService.logout();
            this.props.history.push('/login');
        }
    }

    render() {
        if (this.state.user) {
            return (
                <div>Hello, {this.state.user.firstName}!</div>
            );
        }
        else {
            return (
                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
            );
        }
    }
}