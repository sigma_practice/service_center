﻿import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { authenticationService } from '../services/auth_service';

export class AuthMenu extends Component {
    static displayName = AuthMenu.name;

    constructor(props) {
        super(props);
    }
    
    handleClick(e) {
        e.preventDefault();
        authenticationService.logout();
        window.location.reload(true);
    }

    render() {
        if (authenticationService.isAvailable) {
            return (
                [<NavItem key="0">
                    <NavLink tag={Link} className="text-dark" to="/account">{authenticationService.currentUserValue.user.firstName}</NavLink>
                </NavItem>,
                <NavItem key="1">
                    <NavLink tag={Link} className="text-dark" to={window.location.pathname} onClick={this.handleClick}>Log out</NavLink>
                </NavItem>]
            );
        }
        else {
            return (
                [<NavItem key="0">
                    <NavLink tag={Link} className="text-dark" to="/signup">Sign up</NavLink>
                </NavItem>,
                <NavItem key="1">
                    <NavLink tag={Link} className="text-dark" to="/login">Log in</NavLink>
                </NavItem>]
            );
        }
    }
}