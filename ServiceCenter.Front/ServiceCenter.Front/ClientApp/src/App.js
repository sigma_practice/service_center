import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { Layout } from './components/Layout';

import './custom.css'
import { unregister } from './services/auth_interceptor';
import { Home } from './components/Home';
import { NotFound } from './components/NotFound';
import { LoginPage } from './components/LoginPage';
import { AccountPage } from './components/AccountPage';
import { RegisterPage } from './components/RegisterPage';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' component={LoginPage} />
          <Route path='/account' component={AccountPage} />
          <Route path='/signup' component={RegisterPage} />
          <Route path="*" component={NotFound} />
          {/*
          <Route path='/signup' component={SignupPage} />
          <Route path='/logout' component={LogoutPage} />*/}
        </Switch>
      </Layout>
    );
  }
}
