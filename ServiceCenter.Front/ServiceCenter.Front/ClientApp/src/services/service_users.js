﻿import { BehaviorSubject } from 'rxjs';

import { Config } from './config';
import { authenticationService } from './auth_service';
import { handleResponse } from './auth_response';

export const usersService = {
    getUser
};

function getUser(Token) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${Token}`
        }
    };

    return fetch(`${Config.api_ip}/api/users/info`, requestOptions)
        .then(handleResponse)
        .then(user => {
            return user;
        });
}