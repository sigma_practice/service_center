﻿import { BehaviorSubject } from 'rxjs';

import { Config } from './config';
import { handleResponse } from './auth_response';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authenticationService = {
    login,
    logout,
    register,
    setUserLocal,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value },
    get isAvailable() { return this.currentUserValue && this.currentUserValue.accessToken }
};

function setUserLocal(UserData) {
    var userOld = currentUserSubject.value;
    userOld.user = UserData;
    localStorage.setItem('currentUser', JSON.stringify(userOld));
    currentUserSubject.next(userOld);
}

function register(email, password, firstName, lastName, birthDate) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({ email, password, firstName, lastName, birthDate })
    };

    return fetch(`${Config.api_ip}/api/register`, requestOptions)
        .then(handleResponse)
        .then(user => {
            return user;
        });
}

function login(Email, Password) {
    const requestOptions = {
        method: 'POST',
        headers: { 
			'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
		},
        body: JSON.stringify({ Email, Password })
    };

    return fetch(`${Config.api_ip}/api/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            currentUserSubject.next(user);

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}