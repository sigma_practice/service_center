﻿import fetchIntercept from 'fetch-intercept';
import { authenticationService } from './auth_service';

export const unregister = fetchIntercept.register({
    request: function (url, config) {
        const currentUser = authenticationService.currentUserValue;
        if (currentUser && currentUser.AccessToken) {
            console.log(currentUser.AccessToken);
            config.headers.Authorization = `Bearer ${currentUser.accessToken}`;
        } else {
        }
        return [url, config];
    },

    requestError: function (error) {
        // Called when an error occured during another 'request' interceptor call
        return Promise.reject(error);
    },

    response: function (response) {
        // Modify the reponse object
        return response;
    },

    responseError: function (error) {
        // Handle an fetch error
        return Promise.reject(error);
    }
});