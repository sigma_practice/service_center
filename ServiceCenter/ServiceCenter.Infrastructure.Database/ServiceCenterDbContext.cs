﻿using Microsoft.EntityFrameworkCore;
using ServiceCenter.Domain.Entities;
using System;

namespace ServiceCenter.Infrastructure.Database
{
    public class ServiceCenterDbContext : DbContext
    {
        public ServiceCenterDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(user =>
            {
                user.HasIndex(u => u.Email)
                    .IsUnique();

                user.Property("Notifications")
                    .HasDefaultValue(true);

                user.Property("Status")
                    .HasDefaultValue(UserStatus.Registered)
                    .HasConversion<string>();
            });
            modelBuilder.Entity<UserRequest>(userreq =>
            {
                userreq.HasOne(u => u.user)
                       .WithMany(u => u.userRequsts)
                       .HasForeignKey(u => u.UserId);
                userreq.HasOne(u => u.request)
                       .WithMany(u => u.userRequests)
                       .HasForeignKey(u => u.RequestId);
            });
            modelBuilder.Entity<RequestEdit>(reqedit =>
            {
                reqedit.HasOne(u => u.user)
                       .WithMany(u => u.requestEdits)
                       .HasForeignKey(u => u.UserId);
                reqedit.HasOne(u => u.request)
                       .WithMany(u => u.requestEdits)
                       .HasForeignKey(u => u.RequestId);
            });
            modelBuilder.Entity<Notification>(notif =>
            {
                notif.HasOne(u => u.user)
                .WithMany(u => u.notifications)
                .HasForeignKey(u => u.UserId);
                notif.Property("Read")
                     .HasDefaultValue(true);
            });
            modelBuilder.Entity<Request>(req =>
            {
                req.Property("Status")
                   .HasDefaultValue(RequestStatus.New)
                   .HasConversion<string>();
            });
        }
    }
}
