﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.Constants
{
    public class ControllersConstants
    {
        public const int ItemsOnPageCount = 30;

        public const int CommonResponseCachingDuration = 60;
    }
}
