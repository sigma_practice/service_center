﻿using System;

using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Services.Interfaces;
using ServiceCenter.WebApi.ViewModels.DetailedViewModels;
using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using Microsoft.Extensions.Logging;

namespace ServiceCenter.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private readonly IRequestService requestService;
        private readonly IMapper mapper;

        public RequestsController(IRequestService requestService, IMapper mapper)
        {
            this.requestService = requestService;
            this.mapper = mapper;
        }

        [HttpPost("create")]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> CreateRequestAsync([FromBody] RequestCreateViewModel requestCreateViewModel)
        {
            var request = mapper.Map<Request>(requestCreateViewModel);
            request.Status = RequestStatus.New;
            request.Registered = DateTime.Now;

            await requestService.CreateAsync(request);

            return Ok(mapper.Map<RequestViewModel>(request));
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<RequestViewModel>> GetRequestInfo(int id)
        {
            var request = await requestService.GetByIdAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            var requestViewModel = mapper.Map<RequestViewModel>(request);
            return Ok(request);
        }

    }
}
