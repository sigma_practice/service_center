﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ServiceCenter.Domain.Constants;
using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Helpers;
using ServiceCenter.Infrastructure.Services.Interfaces;
using ServiceCenter.WebApi.Constants;
using ServiceCenter.WebApi.ViewModels.DetailedViewModels;
using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ServiceCenter.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UserRoles.AllUsersRole)]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet("info")]
        [ResponseCache(Location = ResponseCacheLocation.Any,
            VaryByHeader = "Authorization",
            Duration = ControllersConstants.CommonResponseCachingDuration)]
        public async Task<ActionResult<UserDetailedViewModel>> GetUserInfo()
        {
            var userViewModel = await GetCurrentUserAsync();

            if (userViewModel == null)
            {
                return BadRequest();
            }

            return Ok(userViewModel);
        }

        private async Task<UserDetailedViewModel> GetCurrentUserAsync()
        {
            var userId = AuthHelper.GetUserId(User);

            var user = await userService.GetByIdAsync(userId);

            if (user == null)
            {
                return null;
            }

            var userViewModel = mapper.Map<UserDetailedViewModel>(user);

            return userViewModel;
        }
    }
}
