using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceCenter.Infrastructure.Database;
using ServiceCenter.WebApi.CustomMiddleware;
using ServiceCenter.WebApi.Extensions;

namespace ServiceCenter.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ServiceCenterDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("AppDBConnection")));
            //Configure dependency injection for used services
            services.ConfigureInfrastructureServices();
            //Add all existing controllers from the controllers folder
            services.AddControllers();
            //Enable response caching to speed the server up
            services.AddResponseCaching();
            //Add authentication based on tokens and user roles
            services.ConfigureAuthentication(Configuration);
            //Add cross-origin request support from website front to back
            services.ConfigureCors(Configuration);
            //Configure swagger for automatic API testing
            services.ConfigureSwagger();
            //Configure AutoMapper for M-VM mapping
            services.ConfigureMapping();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                //See exceptions in a separate page
                app.UseDeveloperExceptionPage();
                //Use swagger API wrapper
                app.UseSwagger();
                //User swagger UI for in-development testing
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "ServiceCenter API v1");
                });
            }
            //Create a daily rotating logging sink
            loggerFactory.AddFile("Logs/log-{Date}.txt");
            //Add the cross-origin policy
            app.UseCors(Configuration["Policy"]);
            //Allow secure protocol usage
            app.UseHttpsRedirection();
            //Allow controller routing
            app.UseRouting();
            //Allow user authentication
            app.UseAuthentication();
            //Allow user authorization
            app.UseAuthorization();
            //Allow per-request caching
            app.UseResponseCaching();
            //Setup error handling middleware to report json-formatted errors to the dev
            app.UseMiddleware<ErrorHandlingMiddleware>();
            //Setup default endpoints based on controller attributes
            app.UseEndpoints(endpoints =>
            {
                //Map controllers to endpoints using attributes
                endpoints.MapControllers();
            });
        }
    }
}
