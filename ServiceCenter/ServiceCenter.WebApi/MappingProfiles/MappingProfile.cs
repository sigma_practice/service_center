﻿using AutoMapper;
using ServiceCenter.Domain.Entities;
using ServiceCenter.WebApi.ViewModels.DetailedViewModels;
using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;

namespace ServiceCenter.WebApi.MappingProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Map validatable VM to M
            CreateMap<UserRegisterViewModel, User>();
            CreateMap<User, UserDetailedViewModel>();
            CreateMap<Request, RequestViewModel>();
            CreateMap<RequestViewModel, Request>();
            CreateMap<RequestEdit, RequestEditViewModel>();
            CreateMap<RequestEditViewModel, RequestEdit>();
            CreateMap<UserRequest, UserRequestViewModel>();
            CreateMap<UserRequestViewModel, UserRequest>();
            CreateMap<Notification, NotificationViewModel>();
            CreateMap<NotificationViewModel, Notification>();
        }
    }
}
