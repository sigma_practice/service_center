﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using System;
using System.Collections.Generic;

namespace ServiceCenter.WebApi.ViewModels.DetailedViewModels
{
    public class UserDetailedViewModel : BaseViewModel
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Role { get; set; }

        public User Parent { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime Registered { get; set; }

        public UserStatus? Status { get; set; }

        public bool? Notifications { get; set; }
    }
}
