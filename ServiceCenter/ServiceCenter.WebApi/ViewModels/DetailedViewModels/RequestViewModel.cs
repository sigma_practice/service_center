﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.DetailedViewModels
{
    public class RequestViewModel : BaseViewModel
    {
        
        public RequestStatus? Status { get; set; }
        
        public string Description { get; set; }
       
        public DateTime Registered { get; set; }

        public DateTime Ended { get; set; }

        public string Reason { get; set; }
    }
}
