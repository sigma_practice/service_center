﻿using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.DetailedViewModels
{
    public class UserRequestViewModel : BaseViewModel
    {
 
        public int UserId { get; set; }

        public int RequestId { get; set; }

        public int OrderNum { get; set; }

        public bool Approved { get; set; }

        public bool Notify { get; set; }
    }
}
