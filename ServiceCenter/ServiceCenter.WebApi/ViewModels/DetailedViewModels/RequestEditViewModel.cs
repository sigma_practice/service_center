﻿using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.DetailedViewModels
{
    public class RequestEditViewModel : BaseViewModel
    {
        
        public int RequestId { get; set; }

        public int UserId { get; set; }

        public string Comment { get; set; }

        public DateTime TimePerf { get; set; }
    }
}
