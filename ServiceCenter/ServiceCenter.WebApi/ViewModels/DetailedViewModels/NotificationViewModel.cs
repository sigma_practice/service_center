﻿using ServiceCenter.WebApi.ViewModels.SimplifiedViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.DetailedViewModels
{
    public class NotificationViewModel : BaseViewModel
    {
        public int UserId { get; set; }

        public string Title { get; set; }
        
        public string Description { get; set; }

        public bool Read { get; set; }
    }
}
