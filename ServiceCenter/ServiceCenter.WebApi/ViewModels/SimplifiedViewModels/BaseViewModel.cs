﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.SimplifiedViewModels
{
    public class BaseViewModel
    {
        public int Id { get; set; }
    }
}
