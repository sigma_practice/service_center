﻿using System.ComponentModel.DataAnnotations;

namespace ServiceCenter.WebApi.ViewModels.SimplifiedViewModels
{
    public class UserLogintViewModel : BaseViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
