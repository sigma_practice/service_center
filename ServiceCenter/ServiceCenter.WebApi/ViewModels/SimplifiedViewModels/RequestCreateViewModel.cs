﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.WebApi.ViewModels.SimplifiedViewModels
{
    public class RequestCreateViewModel : BaseViewModel
    {
        [Required]
        public string Description { get; set; }

        [Required]
        public string Reason { get; set; }
    }
}
