﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceCenter.WebApi.ViewModels.SimplifiedViewModels
{
    public class UserRegisterViewModel : BaseViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
    }
}
