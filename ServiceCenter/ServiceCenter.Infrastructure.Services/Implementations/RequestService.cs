﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Database;
using ServiceCenter.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServiceCenter.Infrastructure.Services.Implementations
{
    public class RequestService : CrudService<Request, ServiceCenterDbContext>, IRequestService
    {
        public RequestService(ServiceCenterDbContext dbContext) : base(dbContext)
        { 
        }
    }
}
