﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Infrastructure.Services.Implementations
{
    public class UserRequestService : CrudService<UserRequest, ServiceCenterDbContext>
    {
        public UserRequestService(ServiceCenterDbContext dbContext) : base(dbContext)
        {
        }
    }
}
