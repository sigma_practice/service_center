﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Infrastructure.Services.Implementations
{
    public class NotificationService : CrudService<Notification, ServiceCenterDbContext>
    {
        public NotificationService(ServiceCenterDbContext dbContext) : base(dbContext)
        {
        }
    }
}
