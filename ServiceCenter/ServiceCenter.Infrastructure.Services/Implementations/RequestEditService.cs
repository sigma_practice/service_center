﻿using ServiceCenter.Domain.Entities;
using ServiceCenter.Infrastructure.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Infrastructure.Services.Implementations
{
    public class RequestEditService : CrudService<RequestEdit, ServiceCenterDbContext> 
    {
        public RequestEditService(ServiceCenterDbContext dbContext) : base(dbContext)
        {
        }
    }
}
