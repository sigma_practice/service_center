﻿using ServiceCenter.Domain.Entities;
using System.Threading.Tasks;

namespace ServiceCenter.Infrastructure.Services.Interfaces
{
    public interface IAuthenticationService
    {
        Task<string> GetAccessTokenAsync(User user);
        Task<string> RefreshUserTokenAsync(int userId, string oldRefreshToken);
    }
}
