﻿using ServiceCenter.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCenter.Infrastructure.Services.Interfaces
{
    public interface IUserService : ICrudService<User>
    {
        Task<User> GetUserByCredentialsAsync(string email, string password);
    }
}
