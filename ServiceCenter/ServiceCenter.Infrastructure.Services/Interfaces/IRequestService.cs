﻿using ServiceCenter.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Infrastructure.Services.Interfaces
{
    public interface IRequestService : ICrudService<Request>
    {
    }
}
