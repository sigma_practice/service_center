﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Domain.Constants
{
    public static class DbColumnConstraints
    {
        public const int FirstNameLength = 100;

        public const int LastNameLength = 100;

        public const int EmailLength = 320;

        public const int PasswordLength = 128;

        public const int RoleLength = 50;

        public const int CommentLength = 3000;

        public const int TitleLength = 300;

        public const int DescriptionLength = 3000;

        public const int ReasonLength = 300;
    }
}
