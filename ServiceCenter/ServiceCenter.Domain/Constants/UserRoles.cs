﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Domain.Constants
{
    public static class UserRoles
    {
        public const string UserRole = "user";

        public const string DepheadRole = "dephead";

        public const string AdminRole = "admin";

        public const string AllUsersRole = "user, dephead, admin";
    }
}
