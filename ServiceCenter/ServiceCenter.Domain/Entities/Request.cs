﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ServiceCenter.Domain.Constants;

namespace ServiceCenter.Domain.Entities
{
    public class Request : BaseEntity
    {
        [Required]
        public RequestStatus Status { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.DescriptionLength)]
        public string Description { get; set; }

        [Required]
        public DateTime Registered { get; set; }

        public DateTime Ended { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.ReasonLength)]
        public string Reason { get; set; }

        public List<UserRequest> userRequests { get; set; }

        public List<RequestEdit> requestEdits { get; set; }
    }
}
