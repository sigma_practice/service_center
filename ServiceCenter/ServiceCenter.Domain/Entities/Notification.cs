﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ServiceCenter.Domain.Constants;

namespace ServiceCenter.Domain.Entities
{
    public class Notification : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.TitleLength)]
        public string Title { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.DescriptionLength)]
        public string Description { get; set; }

        [Required]
        public bool Read { get; set; }

        public User user { get; set; }
    }
}
