﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceCenter.Domain.Entities
{
    public class UserRequest : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int RequestId { get; set; }

        [Required]
        public int OrderNum { get; set; }

        [Required]
        public bool Approved { get; set; }

        [Required]
        public bool Notify { get; set; }

        public User user { get; set; }
        public Request request { get; set; }

    }
}
