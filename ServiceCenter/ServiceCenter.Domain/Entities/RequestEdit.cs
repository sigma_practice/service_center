﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ServiceCenter.Domain.Constants;

namespace ServiceCenter.Domain.Entities
{
    public class RequestEdit : BaseEntity
    {
        [Required]
        public int RequestId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.CommentLength)]
        public string Comment { get; set; }

        [Required]
        public DateTime TimePerf { get; set; }

        public User user { get; set; }
        public Request request { get; set; }
    }
}
