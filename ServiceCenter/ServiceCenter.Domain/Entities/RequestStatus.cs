﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCenter.Domain.Entities
{
    public enum RequestStatus
    {
        New,
        InProcess,
        Canceled,
        Confirmed,
    }
}
