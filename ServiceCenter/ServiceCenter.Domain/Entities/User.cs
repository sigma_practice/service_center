﻿using ServiceCenter.Domain.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceCenter.Domain.Entities
{
    public class User : BaseEntity
    {
        [Required]
        [StringLength(DbColumnConstraints.EmailLength)]
        public string Email { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.PasswordLength)]
        public string Password { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.RoleLength)]
        public string Role { get; set; }

        [Required]
        public User Parent { get; set; }

        [Required]
        public string RefreshToken { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.FirstNameLength)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(DbColumnConstraints.LastNameLength)]
        public string LastName { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? BirthDate { get; set; }

        [Required]
        [Column(TypeName = "Datetime")]
        public DateTime Registered { get; set; }

        public UserStatus? Status { get; set; }

        public bool? Notifications { get; set; }
        
        public List<UserRequest> userRequsts { get; set; }

        public List<RequestEdit> requestEdits { get; set; }

        public List<Notification> notifications { get; set; }
    }
}
